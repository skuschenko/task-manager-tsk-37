package com.tsc.skuschenko.tm.exception.empty;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! STATUS is empty...");
    }

}
