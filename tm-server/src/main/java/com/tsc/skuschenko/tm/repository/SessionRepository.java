package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.constant.FieldConstant;
import com.tsc.skuschenko.tm.constant.TableConstant;
import com.tsc.skuschenko.tm.model.Session;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;

public final class SessionRepository extends AbstractRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void add(Session entity) {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                "(id, \"timestamp\", signature, user_id)" +
                "VALUES(?,?,?,?)";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setLong(2, entity.getTimestamp());
        statement.setString(3, entity.getSignature());
        statement.setString(4, entity.getUserId());
        statement.execute();
        statement.close();
    }

    @Override
    @SneakyThrows
    protected Session fetch(@Nullable ResultSet row) {
        if (!Optional.ofNullable(row).isPresent()) return null;
        @NotNull final Session session = new Session();
        session.setUserId(row.getString(FieldConstant.USER_ID));
        session.setTimestamp(row.getLong(FieldConstant.TIMESTAMP));
        session.setId(row.getString(FieldConstant.ID));
        session.setSignature(row.getString(FieldConstant.SIGNATURE));
        return session;
    }

    @Override
    protected @NotNull String getTableName() {
        return TableConstant.SESSION_TABLE;
    }

    @SneakyThrows
    public void update(Session entity) {
        @NotNull final String query = "UPDAT " + getTableName() +
                "set id=?, \"timestamp\"=?, signature=?, user_id=?)";
        @NotNull final PreparedStatement statement =
                connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setLong(2, entity.getTimestamp());
        statement.setString(3, entity.getSignature());
        statement.setString(4, entity.getUserId());
        statement.execute();
        statement.close();
    }

}
