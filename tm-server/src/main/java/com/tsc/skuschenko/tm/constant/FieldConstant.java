package com.tsc.skuschenko.tm.constant;

public interface FieldConstant {

    String CREATED = "created";

    String DESCRIPTION = "description";

    String EMAIL = "email";

    String FINISH_DATE = "dateEnd";

    String FIRST_NAME = "firstName";

    String ID = "id";

    String LAST_NAME = "lastName";

    String LOCKED = "locked";

    String LOGIN = "login";

    String MIDDLE_NAME = "middleName";

    String NAME = "name";

    String PASSWORD_HASH = "passwordHash";

    String PROJECT_ID = "project_id";

    String ROLE = "role";

    String SIGNATURE = "signature";

    String START_DATE = "dateBegin";

    String STATUS = "status";

    String TIMESTAMP = "timestamp";

    String USER_ID = "user_id";
}
