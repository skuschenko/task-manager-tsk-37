package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public interface IBusinessRepository<E extends AbstractBusinessEntity>
        extends IRepository<E> {

    E changeStatusById(
            @NotNull String userId, @NotNull String id,
            @NotNull Status status
    );

    E changeStatusByIndex(
            @NotNull String userId, @NotNull Integer index,
            @NotNull Status status
    );

    E changeStatusByName(
            @NotNull String userId, @NotNull String name,
            @NotNull Status status
    );

    void clear(@NotNull String userId);

    E completeById(
            @NotNull String userId, @NotNull String id
    );

    E completeByIndex(
            @NotNull String userId, @NotNull Integer index
    );

    E completeByName(@NotNull String userId,
                     @NotNull String name);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    E findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    E removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    E removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    E removeOneByName(@NotNull String userId, @NotNull String name);

    @SneakyThrows
    E startById(
            @NotNull String userId, @Nullable String id
    );

    E startByIndex(
            @NotNull String userId, @NotNull Integer index
    );

    E startByName(
            @NotNull String userId, @NotNull String name
    );

    E updateOneById(
            @NotNull String userId, @Nullable String id,
            @Nullable String name, @Nullable String description
    );

    E updateOneByIndex(
            @NotNull String userId, @NotNull Integer index,
            @Nullable String name, @Nullable String description
    );

}
