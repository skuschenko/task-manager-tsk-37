package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.api.IRepository;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.model.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    @SneakyThrows
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable String email
    );

    @NotNull
    @SneakyThrows
    User create(
            @Nullable String login, @Nullable String password,
            @Nullable Role role
    );

    @NotNull
    @SneakyThrows
    User create(
            @Nullable String login, @Nullable String password
    );

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findByLogin(@NotNull String login);

    @NotNull
    @SneakyThrows
    User lockUserByLogin(@NotNull String login);

    @Nullable
    User removeByLogin(@NotNull String login);

    @NotNull
    @SneakyThrows
    User setPassword(
            @NotNull String userId, @NotNull String password
    );

    @NotNull
    @SneakyThrows
    User unlockUserByLogin(@NotNull String login);

    @NotNull
    @SneakyThrows
    User updateUser(
            @NotNull String userId, @Nullable String firstName,
            @Nullable String lastName, @Nullable String middleName
    );
}
