package com.tsc.skuschenko.tm.service;


import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.exception.system.ConnectionFailedException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Optional;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public Connection getConnection() {
        @Nullable final String driver = propertyService.getJdbcDriver();
        Optional.ofNullable(driver).orElseThrow(() -> {
            throw new ConnectionFailedException("Driver");
        });
        @Nullable final String userName = propertyService.getJdbcUserName();
        Optional.ofNullable(userName).orElseThrow(() -> {
            throw new ConnectionFailedException("UserName");
        });
        @Nullable final String password = propertyService.getJdbcPassword();
        Optional.ofNullable(password).orElseThrow(() -> {
            throw new ConnectionFailedException("Password");
        });
        @Nullable final String url = propertyService.getJdbcUrl();
        Optional.ofNullable(url).orElseThrow(() -> {
            throw new ConnectionFailedException("Url");
        });
        @NotNull Connection connection =
                DriverManager.getConnection(url, userName, password);
        connection.setAutoCommit(false);
        return connection;
    }

}
