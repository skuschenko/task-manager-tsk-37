package com.tsc.skuschenko.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Session extends AbstractEntity implements Cloneable {

    @Nullable
    private String signature;

    @Nullable
    private Long timestamp;

    @NotNull
    private String userId;

    public Session() {
    }

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
