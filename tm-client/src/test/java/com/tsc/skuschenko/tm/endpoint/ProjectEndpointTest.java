package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;

import java.util.UUID;

public class ProjectEndpointTest {

    @NotNull
    private static final String COMPLETE = "complete";

    @NotNull
    private static final String IN_PROGRESS = "in progress";

    @NotNull
    private static final ProjectEndpointService projectEndpointSrv =
            new ProjectEndpointService();

    @NotNull
    private static final ProjectEndpoint projectEndpoint =
            projectEndpointSrv.getProjectEndpointPort();

    @NotNull
    private static final SessionEndpointService sessionEndpointSrv =
            new SessionEndpointService();

    @NotNull
    private static final SessionEndpoint sessionEndpoint =
            sessionEndpointSrv.getSessionEndpointPort();

    @Nullable
    private static Project project;

    @Nullable
    private static Session session;

    @AfterClass
    public static void afterClassFunction() {
        sessionEndpoint.closeSession(session);
        projectEndpoint.clearProjects(session);
    }

    @BeforeClass
    public static void beforeClassFunction() {
        session = sessionEndpoint.openSession("admin", "admin");
    }

    @Before
    public void beforeFunction() {
        project = projectEndpoint.addProject(session, UUID.randomUUID() + "name1", "des1");
    }

    @Test
    @Category(UnitCategory.class)
    public void testChangeStatusById() {
        Assert.assertNotNull(project);
        projectEndpoint.changeProjectStatusById(
                session, project.getId(), "COMPLETE"
        );
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(COMPLETE, projectFind.getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void testChangeStatusByName() {
        projectEndpoint.changeProjectStatusByName(
                session, project.getName(), "COMPLETE"
        );
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectByName(
                        session, project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                COMPLETE, projectFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testClear() {
        projectEndpoint.clearProject(session);
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testCompleteById() {
        projectEndpoint.completeProjectById(session, project.getId());
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                COMPLETE, projectFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testCompleteByName() {
        Assert.assertNotNull(project.getName());
        projectEndpoint.completeProjectByName(session, project.getName());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectByName(
                        session, project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                COMPLETE, projectFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneById() {
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testFindOneByName() {
        @Nullable final Project projectFind =
                projectEndpoint.findProjectByName(
                        session, project.getName()
                );
        Assert.assertNotNull(projectFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneById() {
        projectEndpoint.removeProjectById(session, project.getId());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByIndex() {
        projectEndpoint.removeProjectByIndex(session, 0);
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testRemoveOneByName() {
        projectEndpoint.removeProjectByName(
                session, project.getName()
        );
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNull(projectFind);
    }

    @Test
    @Category(UnitCategory.class)
    public void testStartById() {
        projectEndpoint.startProjectById(session, project.getId());
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                IN_PROGRESS, projectFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testStartByName() {
        projectEndpoint.startProjectByName(session, project.getName());
        Assert.assertNotNull(project.getStatus());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectByName(
                        session, project.getName()
                );
        Assert.assertNotNull(projectFind.getStatus());
        Assert.assertEquals(
                IN_PROGRESS, projectFind.getStatus()
        );
    }

    @Test
    @Category(UnitCategory.class)
    public void testUpdateOneById() {
        projectEndpoint.updateProjectById(
                session, project.getId(), "name2", "des2"
        );
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        @Nullable final Project projectFind =
                projectEndpoint.findProjectById(session, project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertNotNull(project.getDescription());
        Assert.assertEquals("name2", projectFind.getName());
        Assert.assertEquals("des2", projectFind.getDescription());
    }

}
